import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CaresComponent } from './cares/cares.component';
import { ClientComponent } from './client/client.component';
import { DashboardComponent } from './dashboard/dashboard.component';

const routes: Routes = [
  {path: 'parents', component: ClientComponent},
  {path: 'cares', component: CaresComponent},
  {path: '', component: DashboardComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
