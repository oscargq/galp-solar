import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ServiceService } from '../service.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  public cares:any[] = [];
  public parents: any[] = [];
  public groups: any[] = [];

  constructor(private service: ServiceService,  private router: Router) { 
    this.service.getCares().subscribe((cares) => {
      this.cares = [...cares];
    })
    this.service.getParents().subscribe((parents) => {
      this.parents = [...parents]
    })
  }

  ngOnInit(): void {
    this.groups = [...this.cares];
  }

  public addParent() {
    this.router.navigateByUrl('parents');
  }

  public addCare() {
    this.router.navigateByUrl('cares');
  }

}
