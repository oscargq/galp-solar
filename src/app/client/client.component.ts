import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ServiceService } from '../service.service';

@Component({
  selector: 'app-client',
  templateUrl: './client.component.html',
  styleUrls: ['./client.component.css']
})
export class ClientComponent implements OnInit {

  public padres!: FormGroup;
  public valid: boolean = true;
  public parents: any[] = [];

  constructor
    (private fb: FormBuilder, 
      private service: ServiceService,
      private router: Router) { 
        this.service.getParents().subscribe((response) => {
          this.parents = response;
        })
      }

  ngOnInit(): void {
    this.buildForm();
  }

  private buildForm() {
    this.padres = this.fb.group({
      dni: ['', [Validators.required, Validators.minLength(9), Validators.maxLength(9)]],
      name: ['', [Validators.required]],
      surnames: ['', [Validators.required]],
      hours: ['', [Validators.required]],
      date: ['', [Validators.required]]
    });
  }

  public onSubmit() {
    const padre = this.padres?.value;
    const duplicate = this.noDuplicate(padre);
    const isValid = this.padres?.valid;
    this.valid = isValid;

    if( isValid && !duplicate) {
      this.service.addParents(padre).subscribe((response) => {
        const padres = response;
      });
    }
  }

  public noDuplicate(padre: any) {
    return this.parents?.some((parent: any) => parent.dni === padre.dni);
  }

  public goBack() {
    this.router.navigateByUrl('/');
  }

}
