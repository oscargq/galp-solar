import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { provideRoutes } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { Client } from '../cares/cares.component';
import { ClientComponent } from './client.component';


describe('ClientComponent', () => {
  let component: ClientComponent;
  let fixture: ComponentFixture<ClientComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        ReactiveFormsModule,
        FormsModule 
      ],
      declarations: [ ClientComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ClientComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('crear componenete', () => {
    expect(component).toBeTruthy();
  });

  it('debe retornar un boolean', () => {

    const parent: Client = {
      dni: '4348794L',
      name: 'Mario',
      surnames: 'Sanchez Martinez',
      hours: '2',
      date: '2022-10-07'
    };
    const response = component.noDuplicate(parent);
    expect(response).toBeFalse();
  });
});
