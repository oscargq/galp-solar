import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ServiceService } from '../service.service';

@Component({
  selector: 'app-cares',
  templateUrl: './cares.component.html',
  styleUrls: ['./cares.component.css']
})
export class CaresComponent implements OnInit {

  public cuidados!: FormGroup;
  public valid: boolean = true;
  public AddCare: boolean = false;
  public parents: any[] = [{
    name: '',
    surnames: '',
    dni: '',
    hours: '',
    date: ''
  }];
  public allParents: any[] = [];
  public parentSelected = {};
  public clientSelected = {} as Client;

  constructor(
    private fb: FormBuilder, 
    private service: ServiceService,
    private router: Router) {}

  ngOnInit(): void {
    this.buildForm();
    this.getParentAll();
    const controlDni = this.cuidados.get('dni');
    const controlParent = this.cuidados.get('parents');
    const controlName = this.cuidados.get('name');
    

    controlName?.valueChanges.subscribe((value) => {
      this.clientSelected = this.allParents.find((parent) => value === parent.dni);
      
      if ( this.clientSelected ) {
        controlDni?.setValue(this.clientSelected.dni);
      }
    })

    controlParent?.valueChanges.subscribe(value => {
      const controlHours = this.cuidados.get('hours');
      const controlDate = this.cuidados.get('date');
      const parent = this.allParents.find((parent) => parent.dni === value);
      controlHours?.setValue(parent?.hours);
      controlDate?.setValue(parent?.date);
      this.parentSelected = parent;
    });

    controlDni?.valueChanges.subscribe((value) => {
      this.parentsAviables(value);
    })
  }

  private buildForm() {
    this.cuidados = this.fb.group({
      dni: [{value: '', disabled: true}, [Validators.required, Validators.minLength(9), Validators.maxLength(9)]],
      name: ['',[ Validators.required]],
      parents: [{}, [Validators.required]],
      hours: [{value: '', disabled: false}, [Validators.required]],
      date: [{value: '', disabled: true}, [Validators.required]],
      description: ['',  [Validators.required]]
    });
  }

  public onSubmit() {
    const cuidado = this.cuidados?.value;
    const isValid = this.cuidados?.valid;
    this.valid = isValid;
    if( isValid ) {
      this.service.updateParents(this.cuidados.value);
      this.service.addCares(cuidado).subscribe((response) => {
        const cuidado = response;
        this.AddCare = true
        this.cuidados.reset();
      });
    }
  }

  public getParentAll() {
    this.service.getParents().subscribe((response) => {
      this.allParents = response || [];
    });
  }

  public parentsAviables(dni: any) {

    this.allParents.forEach((parent: any) => {
      const hoursAvailable: boolean = parseInt(parent.hours) >= 1;
      const parentNoSelected: boolean = parent.dni === dni;
      
      if ( hoursAvailable && !parentNoSelected ) {
        this.parents.push(parent);
      }
    });

    const exitsOneParent = this.parents.length === 1;

      if ( exitsOneParent ) {
        const controlHours = this.cuidados.get('hours');
        const controlDate = this.cuidados.get('date');
        controlHours?.setValue(this.parents[0].hours);
        controlDate?.setValue(this.parents[0].date);
      }
  };

  public goBack() {
    this.router.navigateByUrl('/');
  }
}

export interface Client {
  name: string;
  surnames: string;
  dni: string;
  hours: string;
  date: string;
}

