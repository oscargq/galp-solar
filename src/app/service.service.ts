import { Injectable } from '@angular/core';
import { map, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ServiceService {

  public parents: any[] = [
  {
    dni: '4348794L',
    name: 'Jorge',
    surnames: 'Sanchez Martinez',
    hours: '2',
    date: '2022-10-07'
  },
  {
    dni: '54621785J',
    name: 'Sonia',
    surnames: 'Cruz Ramirez',
    hours: '4',
    date: '2022-10-18'
  }];
  public cares: any[]= [{
    date: "2022-10-15",
    description: "Ir al cine",
    dni: "56145874L",
    hours: "3",
    name: "Margarita",
    parents: "Jorge-4348794L"
  },
  {
    date: "2022-10-12",
    description: "Reunión de trabajo",
    dni: "561414785H",
    hours: "3",
    name: "Susana",
    parents: "Jorge-4348794L"
  }];

  constructor() { }

  getParents(): Observable<any> {
   return new Observable((data) => {
    const parents = JSON.parse(<string>localStorage.getItem('parents')) || [];
    data.next(parents);
   })
  }

  updateParents(cuidado: any): Observable<any> {

    if ( cuidado ) {
      const parents = JSON.parse(<string>localStorage.getItem('parents')) || [];
      parents.forEach((value: any) =>{ 
        const existParent = value.dni === cuidado.parents;
        if ( existParent ) {
          value.hours = parseFloat(value.hours) - parseFloat(cuidado.hours);
        }
      });

      localStorage.setItem('parents', JSON.stringify(parents));
    }

    return new Observable((data) => {
      const parents = JSON.parse(<string>localStorage.getItem('parents'));
      data.next(parents);
    })
   }

  addParents(parent: any): Observable<any> {

    if ( parent ) {
      this.parents.push(parent);
      localStorage.setItem('parents', JSON.stringify(this.parents));
    }

    return new Observable((data) => {
      const parents = JSON.parse(<string>localStorage.getItem('parents'));
      data.next(parents);
    })
   }

   getCares(): Observable<any> {
    return new Observable((data) => {
      const cares = JSON.parse(<string>localStorage.getItem('cares')) || [];
      data.next(cares);
    })
   }
 
   addCares(care: any): Observable<any> {

     if ( care ) {
      const parents = JSON.parse(<string>localStorage.getItem('parents')) || [];
      parents.forEach((value: any) =>{ 
        const existName = value.dni === care.name;
        const existParent = value.dni === care.parents;
        if ( existName ) {
          value.name = value.name;
        }
        if ( existParent ) {
          value.parent = value.name;
        }
      });
      this.cares.push(care);
      localStorage.setItem('cares', JSON.stringify(this.cares));
     }
     
     return new Observable((data) => {
      const cares = JSON.parse(<string>localStorage.getItem('cares'));
      data.next(cares);
     })
    }
}
